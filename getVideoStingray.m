function myimages = getVideoStingray(exposureTime, ROIStartX, ROIStopX, ROIStartY, ROIStopY)

%     imaqreset;
    cameras = imaqfind;
    stingray = cameras(1);
    set(stingray,'TriggerRepeat',0);
    triggerconfig(stingray,'immediate','none','none');
    src = getselectedsource(stingray);
    shutter = round((exposureTime - 0.027)/0.02);
    if shutter < 1
        shutter = 1;
    end
    if shutter > 4095
        shutter = 4095;
    end
    set(src,'Shutter',shutter);

    
    start(stingray);
    wait(stingray);
    
    if get(stingray,'FramesAvailable') == 1
        myimages = squeeze(getdata(stingray))';
    else
        myimages = 0;
    end
    
    myimages=myimages/4;
    
    myimages = flipud(myimages);
   
    
%     if strcmp(get(stingray,'VideoFormat'),'F7_Y8_768x492')
%         myimages = deinterlace(myimages);
%     end
    
%     myimages=myimages/4;
    
    clear stingray;
   
%     imaqreset
    
%     % Mode
%     AcquisitionMode = 1;% 1 = single scan, 2 = accumulate (sum scans), 3 = kinetics mode, 4 = fast kinetcs, 5 = run till abort
%     [ret]=SetAcquisitionMode(AcquisitionMode);
% 
%     % Shutter
%     ShutterType = 1; % TTL high
%     ShutterMode = 1; % always open
% 
%     ClosingTime = 0;% ms
%     OpeningTime = 0;% ms
%     [ret]=SetShutter(ShutterType,ShutterMode,ClosingTime,OpeningTime);
% 
%     [ret]=SetTriggerMode(0);%0 = internal, 1 = external
%     
%     % Exposure (in seconds) -- this might be different for fast kinetics?
%     [ret]=SetExposureTime(double(exposureTime/1000));
%     
% %     [ret, exposure, accumulate, kinetic] = GetAcquisitionTimings()
%     
%     x1 = ROIStartX;
%     x2 = ROIStopX;
%     y1 = ROIStartY;
%     y2 = ROIStopY;
% 
%     [ret,xpixels,ypixels]=GetDetector;
%     if (x1 < 1) || (y1 < 1) || (x2 > xpixels) || (y2 > ypixels) || (x1 > x2) || (y1 > y2)
%         x1 = 1; y1 = 1; x2 = xpixels; y2 = ypixels;
%     end
%     xpixels = x2 - x1 + 1;
%     ypixels = y2 - y1 + 1;
%     
% %     if (binningFactor < 1) || (binningFactor > xpixels) || (binningFactor > ypixels)
%         binningFactor = 1;
% %     end
%     [ret]=SetImage(binningFactor,binningFactor,x1,x2,y1,y2);    
% 
%     % Take pictures
%     [ret]=StartAcquisition;
%     [ret,gstatus]=AndorGetStatus;
%     while(gstatus ~= 20073)%DRV_IDLE
%         pause(.1);
%         [ret,gstatus]=AndorGetStatus;
%     end
% 
%     [ret, numstart, numend] = GetNumberNewImages();
%     
%     if  numstart == 1 && numend == 1
%         data = zeros(xpixels*ypixels,1);
%         [ret, data] = GetOldestImage(xpixels*ypixels);
% 
%         myimages = zeros(xpixels,ypixels);
% %         myimages = transpose(reshape(data, xpixels,ypixels));
%         myimages = fliplr(reshape(data, xpixels,ypixels));
%     else
%         myimages = 0;
%     end