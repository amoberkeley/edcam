\documentclass[A4,twoside]{article}
\usepackage[pdftex]{color,graphicx}
\newcommand{\dd}{\mathrm{d}}
\newcommand{\var}{\mathrm{var}\;}
% \newcommand{\edcam}{\texttt{edcam}{}}
\pdfpageheight = \paperheight
\pdfpagewidth = \paperwidth
\title{\texttt{edcam} Documentation}
\author{G. Edward Marti\\emarti@berkeley.edu}

\date{July 2008}


\begin{document}
\maketitle
\tableofcontents
\newpage
\section{Introduction}
All of the information we obtain from our cold atoms experiments comes from light interacting with our atoms. Our samples are very cold and very sensitive to external perturbations, so we must choose between perturbative (dispersive) and destructive (absorptive) measurements.

Most experiments, including the ones \texttt{edcam} is design for, choose the destructive approach. In particular, the main approach is to shine resonant laser light onto the sample, perhaps after the sample has been expanded, and image the light onto a camera. As explained below, we can choose to image the scattered light from the atoms (fluoresence) or the unscattered light from the original laser (absorption). This gives a density distribution from which we can extract static properties such as the number of atoms, temperature, center of mass, and so forth.

\subsection{Why \texttt{edcam}?}
The purpose of \texttt{edcam} is to analyze and extract static properties from a single experimental cycle. \texttt{edcam} contains a batch processing tool that will summarize these properties in a spreadsheet, which can then be used by other analysis software\footnote{I strongly prefer \emph{Igor} and \emph{Matlab}. Any program should be able to read the spreadsheets. However, I have found \emph{Origin} to be subpar for analysis, and no researcher with a modicum of pride would use \emph{Excel}.}. Ideally, image analysis begins and ends with \texttt{edcam}: further analysis is done not on the images but from \texttt{edcam}'s extracted properties.

\texttt{edcam} is written in \emph{Matlab}, a high-level programming language designed for technical computing. It is not particularly fast. However, it is \emph{flexible}. It should be easy to make large changes to the structure of the program, especially the analysis tools.

\subsection{Absorption Imaging}
There are two main approaches to destructive imaging: absorption and fluorescence. In both methods, a resonant laser ('probe') beam illuminated the sample, and an imaging system focuses light from the sample onto a camera. In fluorescence imaging, the light scattered by the atoms is collected, while in absorption imaging, the unscattered light is collected. Hence, in absorption imaging, we look for missing photons to detect the atoms. Absorption imaging typically has a higher signal-to-noise ratio because the entire unscattered beam is collected, while in fluorescence only a small fraction of scattered photons are ever collected. However, both have their advantages and disadvantages.

Here I will only talk about absorption imaging, though \texttt{edcam} should be compatible with both. The lab of David Gu\'ery-Odelin will probably implement fluorescence imaging soon.

\subsubsection{Derivation}
Here I will derive the basic formula for extracting atomic density from an absorption image.

Consider a thin slice of material of thickness $\dd z$ and cross-sectional area $A$. In this material are $N = n(z) A \dd z$ absorbers, where $n(z)$ is the density, each with cross-section $\sigma(I)$, which may be intensity dependent. Light of intensity $I(z)$ passing through the material (through the thin direction, along $z$) is attenuated by the ratio of absorbing area to the total area.

\[ \frac{\dd I}{I} = - \frac{N \sigma(I)}{A} = - n(z) \sigma(I) \dd z \]

Now, let's extend this to a thick sample. In an absorption measurement, I measure the intensity of the light before it enters the sample, $I_0$, and the intensity after it passes through the sample, $I_1$. From this I can infer the column density $\tilde n = N/A$:

\[ N/A = \int \dd z\; n(z) = - \int_{I_0}^{I_1} \frac{\dd I}{I}\;\frac{1}{\sigma(I)} \]

An atom that interacts with resonant light can be modeled very accurately as a two-level system with cross-section $\sigma(I) = \sigma_0/(1+s)$, where $s = I/I_{sat}$, the intensity over the saturation intensity. Substituting $I$ with $s I_{sat}$,

\[ N/A  = - \int_{s_0}^{s_1} \dd s\; \frac{1+s}{s \sigma_0} = \frac{1}{\sigma_0} \left( \log (s_0/s_1) + (s_1 - s_0) \right) \]

The physical meaning of this formula is quite simple. At low intensities ($s \ll 1$), each atom scatters a fraction of the incident photons, and we measure the number of atoms by looking for what percentage of the photons are missing, $N \sim \log(s_0/s_1)$. At high intensities, each atom is completely saturated and scatters photons at a fixed rate $\Gamma/2$, hence the atoms scatter a fixed number of photons. We count atoms by counting the missing photons $s_0 - s_1$. Surprisingly, the crossover regime is simply the sum of the two extremes. (Can anybody give a simple reason why this is true?)

Our camera reads out the counts per pixel $n_a$ in an absorption image (with atoms) and bright-field image $n_b$ (without atoms). (In addition, we need to subtract a dark-field image, which we ignore here). To find the number of atoms, we need to know the atomic cross-section $\sigma_0$ (including errors due to polarization, magnetic field alignment, laser detuning and linewidth, etc.), the area of each pixel sees $A = (\textrm{pixel size}/\textrm{magnification})^2$, the fraction of photons turned into electrons $q$ (which is the camera's quantum efficiency times the optical losses \emph{after} the atoms), the number of counts per electron $g$ (also known as the camera gain), and the pulse length $t$ (we assume a square pulse). 

\[ N = \frac{A}{\sigma_0} \log \frac{n_b}{n_a} + \frac{A}{\sigma_0} \frac{I_b - I_a}{I_{sat}} \]

The light intensity at the atoms may by inferred as the number of photons $n_{a,b}/q g$ per area $A$ times the intensity per photon $\hbar \omega/t$, where $t$ is the pulse time. However, we do not care about the true intensity, only the intensity as compared to the saturation intensity.

\[ \frac{A}{\sigma_0} \frac{I_b - I_a}{I_{sat}} = \frac{\hbar \omega}{I_{sat} \sigma_0} \frac{n_b-n_a}{q g t} \]
Note that $\sigma_0 = \hbar \omega \Gamma/ 2 I_{sat}$. 

\begin{equation}
N = \frac{A}{\sigma_0} \log \frac{n_b}{n_a} + \frac{2}{\Gamma t q g} (n_b - n_a)
\end{equation}

These two terms provide a method of calibrating the imaging system. At low intensity, magnification, probe polarization, and magnetic field alignment contribute to a systematic error in the atom number. These do not effect the high-intensity term because, at high intensities, each atom scatters the name number of photons, regardless of polarization drifts, magnetic field alignment, laser linewidth, magnification, etc.. Instead, the chief error is in $q$, the overall quantum efficiency of the imaging system ($g$ can be measured from the noise in the counts). By taking repeated images of similarly prepared samples, we can measure $(A/\sigma_0)/(2/\Gamma t q g)$. Since $q$ can be independently measured by directly measuring the optical losses, this provides an absolute number calibration of the imaging system.

\begin{figure}[!ht]
\begin{center}
\includegraphics[width=0.7\textwidth]{satscan2.png}
\end{center}
\caption{Calculated number of atoms versus intensity}
\end{figure}

\subsubsection{Noise analysis}
To condense the previous equation, let's rewrite the atom number as
\[ N = A \log \frac{n_b}{n_as} + B (n_b - n_a) \]

From Taylor (3.47):
\begin{eqnarray*}
\delta N^2 & = & \left( \frac{\partial N}{\partial n_b} \delta n_b \right)^2 + \left( \frac{\partial N}{\partial n_a} \delta n_a \right)^2 \\
\var N & = & \left( \frac{A}{n_b} + B \right)^2 \var n_b  + \left( \frac{A}{n_a} + B \right)^2 \var n_a
\end{eqnarray*}

The camera provides counts per pixel $n$, which typically show a sub-Poissonian distribution. The photons $n_p$ arriving on the CCD are (typically) Poisson distributed, and the conversion to electrons $n_e = q_e n_p$ is binomial. Because the number of electrons (and photons) is typically very large, the electron distribution is very close to Poissonian, $(\delta n_e)^2 = n_e$.  The electron conversion to counts (again, large numbers of electrons) is deterministic (plus some constant readout noise). The gain $g$ is defined as the ratio of counts per electron, with $g<1$.

\[ \var n = \var (g n_e) = g^2 \var n_e = g^2 n_e = g n \]

\begin{equation}
\var N = g A^2 \left( \frac{1}{n_b} + \frac{1}{n_a} \right) + g B^2 \left( n_b + n_a \right) + 4 g A B
\end{equation}

Note the important cross-term $4 g A B$: the variance in atom number is \emph{greater} than the sum of the variance of the low and high intensity terms because these terms are correlated. For fixed values of $A$ and $B$, we can minimize the variance with respect to $n = n_b \approx n_a$.

\[ 0 = \frac{\partial \var N}{\partial n_b} = \left( \frac{A}{n_b} + B \right)^2 g - 2 \frac{A}{n_b} \left( \frac{A}{n_b} + B \right) g \quad \Rightarrow \quad n_b = \frac{A}{B} \]
\[ \var N \geq 8 g A B \]

In addition, there is a small amount of noise from the camera read-out that increases the noise by $\var n_{A/D} = g n_{A/D} + e^2$. For our purposes (high photon counts), this noise source is negligible.

\subsection{Experimental notes}
We found that by improving our locks, most notably by adding current feedback, we were able to reduce our laser linewidth to below $400\;\textrm{kHz}$ (inferred from the beatnote between our master and repump lasers) and greatly increase the atom's cross-section from $0.7 \sigma_0$ to greater than $0.95\sigma_0$. We do not know the linewidth of our laser prior to these improvements, but did note that the shot-to-shot jitter (second timescale) was large and on the order of a few megahertz. We found it was best to optimize the laser lock feedback while monitoring the noise on a spectrum analyzer (SRS SR760).

We have been able to minimize fringes by using a small probe beam (mm size), passing through our uncoated glass cell at a large (several degree) angle, and using optics with very good AR coatings (coating /076 from Melles-Griot). Most importantly, introducing frame-transfer imaging reduced most fringes to a negligible level. The measured photon noise within a few percent of shot-noise, though there still persists an unsolved problem of long-term DC drifts (the background level is nonzero and drifts, even with normalization).

\begin{figure}[!ht]
\begin{center}
\includegraphics[width=0.7\textwidth]{noatomsnoise.png}
\end{center}
\caption{Measured atom number uncertainty for experimental shots without atoms.}
\end{figure}


\section{\emph{Matlab} peculiarities}
All counting in \emph{Matlab} starts with \texttt{1}, and \texttt{edcam} always follows this convention. The first element of any vector is \texttt{vec(1)}, the first element of a matrix is \texttt{mat(1,1)}, and so forth. The same goes for loops. Furthermore, in all image matrices, the first index refers to $y$ while the second index refers to $x$. The first (horizontal) row is \texttt{mat(1,:)}.

This may be confusing at first but consistency is best. Please don't change these standards as you edit \texttt{edcam}.

Less peculiar: \texttt{true} is equal to \texttt{1} and \texttt{false} is equal to \texttt{0}. Out of habit, I have usually used \texttt{true} and \texttt{false} in this documentation, but \texttt{1} and \texttt{0} in \texttt{edcam} itself.

\section{Camera control}
The communication between our camera and software makes it tempting to let \texttt{edcam} monitor the camera status and temperature, and give the user GUI control over camera settings. I have resisted this temptation out of the hope that minimal communication allows for the most portable program. Therefore, all commands to the camera are encapsulated in five simple functions that the user can write for each camera: \texttt{arm}, \texttt{disarm}, \texttt{stop}, \texttt{getFrames}, and \texttt{getFT}. If these are not all appropriate for a new camera, they can just be programmed to do nothing and/or fail -- \texttt{edcam} will figure it out.

\subsection{\texttt{arm} and \texttt{disarm}}
These are used to first engage and set the camera and set the modes. They require no inputs. Currently, all communication involving the camera temperature, preamp gain, read-out speed, shift rates, etc. is included in \texttt{arm}. One day some of this information can be moved to a flexible GUI. This should be particularly simple for Andor cameras, which let the user know the details of the various modes (e.g., read-out mode in megahertz, number and value of preamplifier gains). This has not yet proved necessary because the camera modes are essentially chosen once and remain unchanged for months or years. Moreover, this GUI would have to be programmed for each camera model, an onerous and largely unimportant task.

For Andor cameras, the functions are \texttt{armAndor} and \texttt{disarmAndor}.

\texttt{arm} returns pass (returns \texttt{1}) or fail (returns \texttt{0}). \texttt{disarm} does not return anything.

\subsection{\texttt{getFrames}}
\texttt{myimages = getFrames(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)}

\texttt{getFrames} calls a custom Matlab function to communicate with a camera. For Andor cameras, the function is \texttt{getFramesAndor}.

\begin{description} 
\item[\texttt{nOfFrames}] is the number of images the camera will take. It is more efficient to take all of the frames in a single call rather than looping \texttt{getFrames}. This is because the images are only downloaded at the end, and because \texttt{edcam} does not need to trigger the camera for each frame.
\item[\texttt{exposureTime}] is the time in milliseconds allocated for the camera to wait during each exposure. It should in general be much longer than the actual pulse length in case of timing jitter. We typically trigger the camera $100\;\mu\textrm{s}$ before each image and wait for a few hundred microseconds after. Of course, the end time is not so important, since the camera usually waits $10-100\;\textrm{ms}$ for the shutter to close.
\item[\texttt{useTrigger}] The camera will wait for an external trigger if this is \texttt{true}, or it will use an internal trigger if this is \texttt{false}.
\item[\texttt{useShutter}] The camera will trigger its shutter automatically if this is \texttt{true}, or it will keep the shutter closed (under external control) if this is \texttt{false}.
\item[\texttt{ROIStartX},\texttt{ROIStopX},\texttt{ROIStartY},\texttt{ROIStopY}] These define the region of interest of the camera. Like all counting in \texttt{edcam} and \emph{Matlab}, the smallest value is $1$.
\item[\texttt{myimages}] \texttt{getFrames} returns a $y\times x\times \texttt{nOfFrames}$ matrix, where $y = \texttt{ROIStopY}-\texttt{ROIStartY}+1$ and similar for $x$. The third dimension is numbered from first exposure (\texttt{1}) to last (\texttt{nOfFrames}).
\end{description}

The custom function for each camera should be programmed to handle errors and return \texttt{myimages=0} upon failure.

A brief note on Andor cameras: it is nontrivial to program the camera to alert \texttt{edcam} when an acquisition is complete. Instead, \texttt{edcam} continuously polls the camera. This should not cause problems because the polling is fairly rare.

\subsection{\texttt{getFT}}
\texttt{myimages = getFT(nOfFrames, framesPerCCD, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)}

\texttt{getFT} is essentially identical to \texttt{getFrames}, only it uses the camera's frame-transfer mode. For Andor cameras, the function is \texttt{getFTAndor}. There are two slight differences. 

\begin{description}
\item[\texttt{framesPerCCD}] The CCD is divided into strips of dimension $x \times (y/\texttt{framesPerCCD})$, where $x$ and $y$ are the dimensions of the full CCD. We typically mask the bottom half and set \texttt{framesPerCCD = 2}.
\item[\texttt{nOfFrames}] is the number of $x \times (y/\texttt{framesPerCCD})$ strips the camera will return. If we set \texttt{framesPerCCD = 2}, then \texttt{nOfFrames = 2} will return one full CCD, while \texttt{nOfFrames = 1} will return only half of the CCD. Like $\texttt{getFrames}$, the images are ordered from first exposure to last.
\end{description}

The custom function for each camera should be programmed to handle errors and return \texttt{myimages=0} upon failure.

A brief note on Andor cameras: Like \texttt{getFramesAndor}, \texttt{getFTAndor} continuously polls the camera. However, in frame-transfer (``fast-kinetics'') mode, Andor only allows the user to acquire one full frame (up to $\texttt{nOfFrames} = \texttt{framesPerCCD}$) at a time. After each frame, the user must download the image data and demand a new acquisition. This causes a real problem when $\texttt{nOfFrames} > \texttt{framesPerCCD}$, as the camera must be polled very rapidly to ensure that \texttt{edcam} does not miss a shot. A busy CPU aggravates the problem. The user must leave ample time between different full-frame exposures to prevent this. We have found an additional $200-400\;\textrm{ms}$ is typically enough, but it does depend on the computer.


\subsection{\texttt{stop}}
This triggers the acquisition (\texttt{getFrames} or \texttt{getFT}) to fail. Upon failure, any existing \texttt{getFrames} or \texttt{getFT} should return \texttt{0}, the standard failure command.

For Andor cameras, the function is \texttt{stopAndor}.

\section{Global variables}

\subsection{\texttt{temp}}
\subsection{\texttt{frames}}
\subsection{\texttt{camera}}
\verb+camera+ contains all of the camera specific information.
\subsubsection{\texttt{camera.pixelsize}} \texttt{camera.pixelsize} is the length that one pixels sees \emph{at the atoms} in microns, and is therefore the camera's actual pixel size in microns divided by the magnification. This can be calibrated most easily by time-of-flight data of a BEC. In the imaging section, $A = (\texttt{camera.pixelsize})^2$.
\subsubsection{\texttt{camera.efficiency}} \texttt{camera.efficiency} is the total losses of the system, including the camera's quantum efficiency. It is defined as number of electrons registered by the CCD divided by the number of photons \emph{at the atoms}. In the imaging section, $q = \texttt{camera.efficiency} < 1$.
\subsubsection{\texttt{camera.gain}} \texttt{camera.gain} is the number of counts registered by the camera per electron. In theory, the noise of this conversion is independent of the number of electrons and in particular is not Poissonian. In the imaging section, $g = \texttt{camera.gain} < 1$. Usually, we measure the number of counts per photon, $\texttt{camera.gain}*\texttt{camera.efficiency} = gq $.
\subsubsection{\texttt{camera.crosssection}} \texttt{camera.crosssection} is the actual (experimental) atomic cross-section in square microns. For the D2 transition of ${}^{87}$Rb, the classical cross-section is $0.2907\;\mu\textrm{m}^2$, though the experimental value may be much less than that. In the imaging section, $\sigma = \texttt{camera.crosssection} < \sigma_0 = 0.2907\;\mu\textrm{m}^2$.
\subsubsection{\texttt{camera.linewidth}} \texttt{camera.linewidth} is the natural linewidth, or decay rate, of the atom transition in megahertz. For the D2 transition of ${}^{87}$Rb, the linewidth is $2\pi \times 6.07\;\textrm{MHz} = 38.11 \;\textrm{MHz}$. Since we always image samples that are well below the Doppler temperature, the literature value should be accurate. In the imaging section, $\Gamma = \texttt{camera.linewidth}$.
\subsubsection{\texttt{camera.pulselength}} We usually excite our atoms with a nearly square pulse. In this approximation, \texttt{camera.linewidth} is pulse length in microseconds. In the imaging section, $t = \texttt{camera.pulselength}$.
\subsection{\texttt{view}}
\subsection{\texttt{batch}}
\subsection{\texttt{handles}}
\texttt{handles} are not global variables at all, but rather part of the structure of the GUI. This makes them more difficult to pass and update.

\end{document}