%Just copy and paste this entry over and over. Put the filesnames (like
%'andor.mat') into edcam: popupmenu_camera_Callback

%     cd([ matlabroot '\\toolbox\\edcam\\current'])
    cd('c:\\program files\\matlab\\r2008b\\toolbox\\edcam\\beta')

atom = struct;
atomdata = struct;
    atom.name = 'Rb';
    atom.sigma0 = 0.2907; %0.2907 microns^2
    atom.linewidth = 38.11; % 2*pi*6.065 megahertz
    atom.detuning = 0; %resonant detuning in megahertz (no 2*pi)
    atom.sat = 0; % I/Isat -- perhaps this should be in mW/cm^2?
    atom.efficiency = 0.57;
    atom.crosssection = 0.2907;
    atom.scatteringrate = 38.11/2;
    atomdata = atom;
    
    atom.name = 'Li';
    atom.sigma0 = 0.2150;
    atom.linewidth = 36.90;
    atom.detuning = 0;
    atom.sat = 0;
    atom.efficiency = 0.8;
    atom.crosssection = 0.2150;
    atom.scatteringrate = 36.90/2;
    atomdata(2) = atom;
    
    atom.name = 'Rb (MOT)';
    atom.sigma0 = 0.1355;
    atom.linewidth = 38.11;
    atom.detuning = 26;
    atom.sat = 19;
    atom.efficiency = 0.57;
    atom.crosssection = 0.1355;
    atom.scatteringrate = 38.11/2;
    atomdata(3) = atom;
    
    atom.name = 'Li (MOT)';
    atom.sigma0 = 0.1003;
    atom.linewidth = 36.90;
    atom.detuning = 20;
    atom.sat = 8.6;
    atom.efficiency = 0.8;
    atom.crosssection = 0.1003;
    atom.scatteringrate = 36.90/2;
    atomdata(4) = atom;
    
    atom = atomdata(1);
    
    save('atomdata.mat','atom','atomdata');
    
    % Li cross-section: 0.2150 for sigma+, 0.1003 for randomly polarized
    % (random = maximum*7/15)
    % Rb cross-section: 0.2907 for sigma+, 0.1356 for randomly polarized
    % (Steck) or 0.12 (Lewandowski)