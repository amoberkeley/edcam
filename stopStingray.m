function stopStingray()
    cameras = imaqfind;
    stingray = cameras(1);
    stop(stingray);
    clear stingray;