function [label] = displaystruct(mystruct)

label = [];
if not(isempty(struct2cell(mystruct)))
    fn = fieldnames(mystruct);
    
    for i=1:length(mystruct)
        if length(mystruct) > 1
            num = num2str(i);
        else
            num = '';
        end

        for n=1:length(fn)
            if ischar(mystruct(i).(fn{n}))
                label = [ label sprintf('%s%s: %s\n',fn{n},num,mystruct(i).(fn{n})) ];
            else
                label = [ label sprintf('%s%s: %0.4G\n',fn{n},num,mystruct(i).(fn{n})) ];
            end
        end
    end
    label = [ label sprintf('\n') ];
end