%in = scalar, vector, matrix, whatever
%out = each entry 
function out = pnoise(in)

out = zeros(size(in)) - 1;
p = zeros(size(in));
q = zeros(size(in));

temp = (p <= in);
while any(temp)
    q = find(temp);
    for i=1:length(q)
        p(q(i)) = p(q(i)) - log(rand());
    end
    out = out + temp;
    temp = (p <= in);
end

% Use 'find' command