function pass = armStingray()
    imaqreset;
    stingray = videoinput('dcam',1); % On the default mode, the camera returns a 16-bit number by multiplying the data by 4.
         % Sample code to see this:
%          shot=getFramesStingray(1,10,0,0,1,1,1,1);
%          test=double(reshape(shot,size(shot,1)*size(shot,2),1));
%          mydist=histc(test,(median(test)-100):(median(test)+100));plot(mydist,'.');mydist
%          test2=round(test/4)-test/4;find(test2)
%     stingray = videoinput('dcam',1,'F7_Y16_780x580'); %this mode does not work
%     stingray = videoinput('dcam',1,'F7_Y16_388x580');
    set(stingray,'FramesPerTrigger',1);
    set(stingray,'TimeOut',1200);
%     stingray.TimerFcn = {'stop'};
%     triggerconfig(stingray,'hardware','risingEdge','externalTrigger');
    src = getselectedsource(stingray);
    set(src,'Gain',0); %0 to 680
    set(src,'GainMode','manual');
    set(src,'AutoExposure',100);
    set(src,'ShutterMode','manual');
    clear stingray;
    pass = 1; %debug later; 