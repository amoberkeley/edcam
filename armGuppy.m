function pass = armGuppy()
    imaqreset;
%     guppy = videoinput('dcam',1,'F7_Y8_768x492');
    guppy = videoinput('dcam',1);
    set(guppy,'FramesPerTrigger',1);
    set(guppy,'TimeOut',1200);
%     guppy.TimerFcn = {'stop'};
    triggerconfig(guppy,'hardware','fallingEdge','externalTrigger');
    src = getselectedsource(guppy);
    set(src,'Gain',0); %0 to 680
    set(src,'GainMode','manual');
    set(src,'AutoExposure',100);
    set(src,'ShutterMode','manual');
    clear guppy;
    pass = 1; %debug later;