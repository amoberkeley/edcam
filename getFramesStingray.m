function myimages = getFramesStingray(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)
    ROIStartX = 1; ROIStopX = 388;
    ROIStartY = 1; ROIStopY = 290;
    xpixels = ROIStopX - ROIStartY + 1;
    ypixels = ROIStopY - ROIStartY + 1;

%     imaqreset;
    cameras = imaqfind;
    stingray = cameras(1);
    src = getselectedsource(stingray);
    
    set(stingray,'TriggerRepeat',nOfFrames-1);
    
    shutter = round((exposureTime - 0.027)/0.02);
    if shutter < 1
        shutter = 1;
    end
    if shutter > 4095
        shutter = 4095;
    end
    set(src,'Shutter',shutter);
    if useTrigger
        triggerconfig(stingray,'hardware','fallingEdge','externalTrigger');
    else
        triggerconfig(stingray,'immediate','none','none');
    end
    
    start(stingray);
%     'waiting'
    wait(stingray);

%     myimages = zeros(ypixels,xpixels,nOfFrames);
    if (get(stingray,'FramesAvailable') == nOfFrames)
        for i=1:nOfFrames
%             [ myimages(:,:,i) timeData metaData ] = getdata(stingray);
            myimages(:,:,i) = getdata(stingray)'; %Rotate image
%             datestr(metaData.AbsTime)
%             datestr(metaData.AbsTime,'FFF')

%             myimages(:,:,i) = myimages(:,:,i)'; %Rotate image
        end
        'got frames!'
    else
        myimages = 0;
        'nope, nothing'
    end

    myimages = myimages/4; % Original data is multiplied by 4.
%     myimages = myimages'; % Rotate
%     if strcmp(get(stingray,'VideoFormat'),'F7_Y8_768x492')
%         myimages = deinterlace(myimages);
%     end

    clear stingray;

%     imaqmem;ans.FrameMemoryUsed
    
    
    % Fix Look-Up Table <-- DOUBLE-CHECK THIS!!!
    
%     myimages=double(uint16(myimages).^2);
    
    