function writestruct(filename,mystruct)

fid = fopen(filename,'wt');
mycell = squeeze(struct2cell(mystruct));
fn = fieldnames(mystruct);
for n=1:(length(fn)-1)
    fprintf(fid,'%s,',fn{n});
end
fprintf(fid,'%s',fn{n+1}); %get rid of the last comma
fprintf(fid,'\n');

for m=1:size(mycell,2);
    for n = 1:(size(mycell,1));
        if ischar(mycell{n,m})
            fprintf(fid,'%s',mycell{n,m});
        else
            fprintf(fid,'%0.6G',mycell{n,m});
        end
        if n < size(mycell,1);
            fprintf(fid,',');
        else
            fprintf(fid,'\n');
        end
    end
end
fclose(fid);