function stopGuppy()
    cameras = imaqfind;
    guppy = cameras(1);
    stop(guppy);
    clear guppy;