function myimages = getFramesAndor(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)

    % Mode
    AcquisitionMode = 3;% 1 = single scan, 2 = accumulate (sum scans), 3 = kinetics mode, 4 = fast kinetcs, 5 = run till abort
    [ret]=SetAcquisitionMode(AcquisitionMode);
    [ret]=SetNumberKinetics(nOfFrames);

    % Shutter
    ShutterType = 1;% TTL high
    if useShutter == 0
        ShutterMode = 2;% 0 = Auto, 2 = always closed
    else
        ShutterMode = 0;% 0 = Auto, 2 = always closed
    end
    ClosingTime = 100;% ms
    OpeningTime = 300;% ms
    [ret]=SetShutter(ShutterType,ShutterMode,ClosingTime,OpeningTime);

    % Trigger
    if not(useTrigger == 0)
        useTrigger = 1;
    end
    [ret]=SetTriggerMode(useTrigger);%0 = internal, 1 = external
    
    % Exposure (in seconds) -- this might be different for fast kinetics?
    [ret]=SetExposureTime(double(exposureTime/1000));
    [ret]=SetKineticCycleTime(0);
    
%     [ret, exposure, accumulate, kinetic] = GetAcquisitionTimings()
    
    % Image Size -- I need to rotate since Matlab and Andor disagree on X
    % and Y
%     y1 = ROIStartX; %I count from 1, Pascal counts from 0
%     y2 = ROIStopX;
%     x1 = ROIStartY;
%     x2 = ROIStopY;
    x1 = ROIStartX; %I count from 1, Pascal counts from 0
    x2 = ROIStopX;
    y1 = ROIStartY;
    y2 = ROIStopY;


    
    [ret,xpixels,ypixels]=GetDetector;
    if (x1 < 1) || (y1 < 1) || (x2 > xpixels) || (y2 > ypixels) || (x1 > x2) || (y1 > y2)
        x1 = 1; y1 = 1; x2 = xpixels; y2 = ypixels;
    end
    xpixels = x2 - x1 + 1;
    ypixels = y2 - y1 + 1;
    
%     if (binningFactor < 1) || (binningFactor > xpixels) || (binningFactor > ypixels)
        binningFactor = 1;
%     end
    [ret]=SetImage(binningFactor,binningFactor,x1,x2,y1,y2);    
    data = zeros(xpixels*ypixels,nOfFrames);

    % Take pictures
    [ret]=StartAcquisition;
    [ret,gstatus]=AndorGetStatus;
    while(gstatus ~= 20073)%DRV_IDLE
        pause(1);
        [ret,gstatus]=AndorGetStatus;
    end

    [ret, numstart, numend] = GetNumberNewImages();
    if (numend - numstart + 1 == nOfFrames)
        for k = numstart:numend
            [ret, data(:,k) ] = GetOldestImage(xpixels*ypixels);
        end

        myimages = zeros(ypixels,xpixels,numend-numstart+1);

        for k = numstart:numend
            myimages(:,:,k) = transpose(reshape(data(:,k), xpixels,ypixels));
        end
    else
        myimages = 0;
    end

%     picBuff = squeeze(reshape(data,size(data,1)*size(data,2)*size(data,3),1,1));
    
%     %Take background
%     if not(takebackground == 0)
%         [ret]=SetShutter(ShutterType,2,ClosingTime,OpeningTime);
%         [ret]=SetNumberKinetics(1);
%         [ret]=StartAcquisition;
%         [ret,gstatus]=AndorGetStatus;
%         while(gstatus ~= 20073)%DRV_IDLE
%             pause(0.5);
%             [ret,gstatus]=AndorGetStatus;
%         end
%         numend = numend + 1;
%         [ret, data(:,numend) ] = GetOldestImage(xpixels*ypixels);
%         [ret]=SetShutter(ShutterType,ShutterMode,ClosingTime,OpeningTime);
%     end
    

    
